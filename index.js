require('dotenv').config();

const keys = require('./config/keys.js');

const express = require('express'); //luu tru goi express vao 1 bien khong doi (const) express
const app = express();

const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

//Setting up DB
const mongoose = require('mongoose');
const { query } = require('express');
//mongoose.connect('mongodb://localhost:27017/test');
mongoose.connect(keys.mongoURI);

//Setup database models
require('./model/Account');
//Setup the routes
require('./routes/authenticationRoutes')(app);

app.listen(keys.port, () => {
    console.log("Listening on PORT: " + keys.port);
});