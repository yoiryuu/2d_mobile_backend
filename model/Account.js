const mongoose = require('mongoose');

const { Schema } = mongoose;

const accountSchema = new Schema({
    username: String,
    password: String,
    total: Number,
    win: Number,
    kill: Number,
    salt: String,
    accessToken: String,
    lastAuthentication: Date,
})

mongoose.model('account', accountSchema);