const mongoose = require('mongoose');

const { Schema } = mongoose;

const leaderboardSchema = new Schema({
    name: String,
    score: String,
})

mongoose.model('leaderboard', leaderboardSchema);