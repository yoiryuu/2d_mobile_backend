const mongoose = require('mongoose');
const Leaderboard = mongoose.model('leaderboard');

module.exports = app => {
    /* Routes */
    // get LEADERBOARD
    app.get('/leaderboard/get', async (req, res) => { //resond phan hoi, require yeu cau, async khong dong bo
        Leaderboard.find().sort({ score: -1 }).limit(10).then(function (top10) {
            res.send(top10);
        });
    });

    // add LEADERBOARD
    app.get('/leaderboard/add', async (req, res) => { //resond phan hoi, require yeu cau, async khong dong bo
        const { newName, newScore } = req.query;
        var newLB = new Leaderboard({
            name: newName,
            score: newScore,
        });
        await newLB.save();
        Leaderboard.find().sort({ score: -1 }).limit(10).then(function (top10) {
            res.send(top10);
        });
    });
}