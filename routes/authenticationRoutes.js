const mongoose = require('mongoose');
const Account = mongoose.model('account');

const argon2i = require('argon2-ffi').argon2i;
const crypto = require('crypto');
const res = require('express/lib/response');

const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();

module.exports = app => {
    /* Routes */

    //  LOGIN
    app.post('/account/login', async (req, res) => { //resond phan hoi, require yeu cau, async khong dong bo
        //Authentication - xac thuc tai khoan
        var response = {};

        const { rUsername, rPassword } = req.body;
        //khong duoc de trong
        if (rUsername == null || rPassword == null) {
            response.code = 1;
            response.msg = "Invalid credentials";
            res.send(response);
            return;
        }

        //Authorization - uy quyen
        //tao token
        const accessToken = jwt.sign({ userAccount }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30s' });

        var userAccount = await Account.findOne({ username: rUsername });
        if (userAccount != null) {
            argon2i.verify(userAccount.password, rPassword).then(async (success) => {
                if (success) {
                    userAccount.lastAuthentication = Date.now();
                    userAccount.accessToken = accessToken;

                    await userAccount.save();
                    console.log("Retrieving account...");

                    response.code = 0;
                    response.msg = "Account found";
                    response.data = userAccount;
                    res.send(response);
                    return;
                }
                else {
                    //neu sai mat khau thi tra ve loi
                    response.code = 1;
                    response.msg = "Invalid credentials";
                    res.send(response);
                    return;
                }
            });
        }
        else {
            //neu khong co tai khoan nao
            response.code = 1;
            response.msg = "Invalid credentials";
            res.send(response);
            return;
        }
    });

    // CREATE
    app.post('/account/create', async (req, res) => { //resond phan hoi, require yeu cau, async khong dong bo

        var response = {};

        const { rUsername, rPassword } = req.body;
        //khong duoc de trong
        if (rUsername == null || rPassword == null) {
            response.code = 1;
            response.msg = "Invalid credentials";
            res.send(response);
            return;
        }

        var userAccount = await Account.findOne({ username: rUsername });
        if (userAccount == null) {
            //create a new account
            console.log('Create a new acount...');

            //Generate a unique access token
            crypto.randomBytes(32, function (err, salt) {
                if (err) {
                    console.log(err);
                }

                argon2i.hash(rPassword, salt).then(async (hash) => {
                    var newAccount = new Account({
                        username: rUsername,
                        password: hash,
                        total: 0,
                        win: 0,
                        kill: 0,
                        salt: salt,
                        accessToken: '',
                        lastAuthentication: Date.now()
                    });
                    await newAccount.save();

                    response.code = 0;
                    response.msg = "Account found";
                    response.data = newAccount;
                    res.send(response);
                    return;
                });
            });
        }
        else {
            response.code = 2;
            response.msg = "Username is already taken";
            res.send(response);
        }
        return;
    });
    //  Update statistic
    app.post('/account/update', async (req, res) => { //resond phan hoi, require yeu cau, async khong dong bo
        //Authentication - xac thuc tai khoan
        var response = {};

        const { rUsername, rTotal, rWin, rKill } = req.body;

        var userAccount = await Account.findOne({ username: rUsername });
        if (userAccount != null) {
            userAccount.total = rTotal;
            userAccount.win = rWin;
            userAccount.kill = rKill;

            await userAccount.save();
            console.log("Update account...");

            response.code = 0;
            response.msg = "Account updated";
            response.data = userAccount;
            res.send(response);
            return;
        }
        else {
            //neu khong co tai khoan nao
            response.code = 1;
            response.msg = "Invalid credentials";
            res.send(response);
            return;
        }
    });

    //Authentication Token
    function authenToken(req, res, next) {
        const token = req.header('auth-token');
        if (!token) res.sendStatus(401).send('Access Denied');
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
            console.log(err, data);
        });
    }
}
